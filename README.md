# A collection for installing platform components on Kubernetes

This repo contains the `orange.k8s_platform` Ansible Collection. The collection
includes roles dedicated to "PaaS" components installation on top of a running
Kubernetes.

## External requirements

`external_requirements` role groups all external requirements.
This role doesn't need privilege escalation.

## Collection dependencies

This collection uses `kubernetes.core` collection when dealing with
Kubernetes resources.

## Included content

The collection provides the following roles:

- external_requirements
- longhorn (storage class)
- elasticsearch
- fluentd
- vault

## Using this collection

Before using the General community collection, you need to install the
collection with the `ansible-galaxy` CLI:

```shell
ansible-galaxy collection install https://gitlab.com/OrangeLabsNetworks/k8s-platform-collection.git
```

use `,branch_or_tag` if you want to use a specific branch version:

```shell
ansible-galaxy collection install https://gitlab.com/OrangeLabsNetworks/k8s-platform-collection.git,v1
```

You can also include it in a `requirements.yml` file and install it via
`ansible-galaxy collection install -r requirements.yml` using the format:

```yaml
collections:
  - name: git+https://gitlab.com/OrangeLabsNetworks/k8s-platform-collection.git
    type: git
    version: master
```

See [Ansible Using collections](
  https://docs.ansible.com/ansible/latest/user_guide/collections_using.html) for
more details.

See [docs folder](docs/) for detailed doc per role and playbooks
