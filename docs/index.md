# Kubernetes platform collection

The goal of this collection consists in deploying
kubernetes addons simultaneously or separately.

It consists mainly in one single [playbook](./playbooks/deploy.md)
leveraging several roles.
Each role corresponds to an add-on.

## Playbooks

| Playbook                        | Description                            |
| ------------------------------- | -------------------------------------- |
| [deploy](./playbooks/deploy.md) | Playbook to deploy kubernetes addons   |
| test                            | Playbook use for the collection gating |

## Roles
<!-- markdownlint-disable line-length -->
| Role                                                      | Description                                  |
| --------------------------------------------------------- | -------------------------------------------- |
| [elasticsearch](./roles/elasticsearch.md)                 | Installation of the search engine            |
| [fluentd](./roles/fluentd.md)                             | Installation of the logging management tool  |
| [longhorn](./roles/longhorn.md)                           | Installation of a kubernetes storage class   |
| [prometheus](./roles/prometheus.md)                       | Installation of the prometheus alert manager |
| [vault](./roles/vault.md)                                 | Installation of the vault security tool      |
| [external requirements](./roles/external_requirements.md) | Management of external requirements          |
<!-- markdownlint-enable line-length -->

## Versions

| ORONAP version | Collection sha1                              |
| -------------- | -------------------------------------------- |
| Honfleur       | b0af47a12afb69c382dc2e69e271d8d3496ee30b     |
| Istres         | b0af47a12afb69c382dc2e69e271d8d3496ee30b     |
