# Longhorn

> ⚠️ This role has moved to k8s collection so you should use it directly

## Purpose

Install longhorn Storage Class.
See [longhorn documentation](https://longhorn.io/docs/1.1.2/) for a complete
view of this component.

## Parameters

See [fluentd role documentation in kubernetes collection](
https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/-/blob/master/docs/roles/longhorn.md)
for a complete description.

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.k8s_platform.longhorn

- ansible.builtin.import_role:
    name: orange.k8s_platform.longhorn
  vars:
    kubernetes_charts_longhorn_version: 1.0.2
```
