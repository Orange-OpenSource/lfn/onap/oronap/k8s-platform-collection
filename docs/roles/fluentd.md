# Fluentd

> ⚠️ This role has moved to k8s collection so you should use it directly

## Purpose

As `elastic operator` installed in the role `elasticsearch` installs only the
database part of logging, this role handles logs retrieval and push to the
database.

This role installs [`banzaicloud logging-operator`](
https://banzaicloud.com/docs/one-eye/logging-operator/) and uses it to install
`fluentbit` and `fluentd` on the cluster.

## Parameters

See [fluentd role documentation in kubernetes collection](
https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/-/blob/master/docs/roles/fluentd.md)
for a complete description.

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.k8s_platform.fluentd

- ansible.builtin.import_role:
    name: orange.k8s_platform.fluentd
  vars:
    kubernetes_charts_operator_namespace: monitoring-tools
    kubernetes_charts_fluentd_namespace: fluent-namespace
    # use default configuration of the logging operator
    k8s_platform_logging_buffer: {}

# Using fluentd role with a custom ES install
- ansible.builtin.import_role:
    name: orange.k8s_platform.fluentd
  vars:
    k8s_platform_fluentd: {} # use default configuration for fluentd
    k8s_elasticsearch_in_fluentd_role_vars:
      ## Elasticsearch related
      kubernetes_charts_elasticsearch_name: elastic-custom
      kubernetes_charts_elasticsearch_user: elastic-user
      kubernetes_charts_elasticsearch_namespace: logging-ns
      kubernetes_charts_elasticsearch_internal_user_secret_key: elastic
      # kubernetes_charts_elasticsearch_storage_class:
      ## Kibana related
      kubernetes_charts_kibana_name: kibana-custom
      # kubernetes_charts_kibana_host should be in etc/hosts
      kubernertes_charts_ingress_domain: some.domain.com
      kubernetes_charts_kibana_host:
        "kibana.{{ kubernertes_charts_ingress_domain }}"
      # kubernetes_charts_kibana_service_type: ClusterIP (default)"
```
