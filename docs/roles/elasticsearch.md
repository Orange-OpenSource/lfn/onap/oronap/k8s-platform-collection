# `Elasticsearch`-`Kibana`

> ⚠️ This role has moved to k8s collection so you should use it directly

## Purpose

This role installs [`Elastic operator`](
https://www.elastic.co/guide/en/cloud-on-k8s) and use it to install
`Elasticsearch` and `Kibana` on the cluster.

Please take look at `Elastic operator` [documentation (version 1.7.1)](
https://www.elastic.co/guide/en/cloud-on-k8s/1.7/k8s-operating-eck.html) or its
[Github repository](
https://github.com/elastic/cloud-on-k8s/tree/1.7.1/deploy/eck-operator) for
more information.

## Parameters

See [elasticsearch role documentation in kubernetes collection](
https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/-/blob/master/docs/roles/elasticsearch.md)
for a complete description.

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.k8s_platform.elasticsearch


- ansible.builtin.import_role:
    name: orange.k8s_platform.elasticsearch
  vars:
    kubernetes_charts_elastic_version: 1.4.1
    kubernertes_charts_ingress_domain: yourdomain.com
    kubernetes_charts_kibana_host: somename.{{kubernertes_charts_ingress_domain}}
    kubernetes_charts_kibana_service_type: NodePort

# kubernetes_charts_kibana_host must be added to the hostfile /etc/hosts/
```
