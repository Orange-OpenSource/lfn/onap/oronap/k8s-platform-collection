# Prometheus Stack (Prometheus, Alertmanager, Grafana)

> ⚠️ This role has moved to k8s collection so you should use it directly

## Purpose

Install `Prometheus`, `Alertmanager` and `Grafana` using prometheus-stack Helm
chart.

For more information about Prometheus Stack Helm Chart,
go to [prometheus-stack](
https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack)

This Helm chart will install `kube-prometheus`: a collection of manifests to
monitor a kubernetes cluster.

For more information about these manifests,
go to [kube-prometheus](
https://github.com/prometheus-operator/kube-prometheus)

The deployment of prometheus uses a kubernetes operator named
`Prometheus Operator`

To fully understand the difference between:

- prometheus-stack
- kube-prometheus
- prometheus-operator

 go to [prometheus Operator](
https://github.com/prometheus-operator/prometheus-operator)

## Upgrade

If you plan to upgrade the chart, please go to
[prometheus-stack upgrading chart](
https://github.com/prometheus-community/helm-charts/tree/kube-prometheus-stack-15.1.1/charts/kube-prometheus-stack#upgrading-chart)
because CRDs are not updated by default and should be manually updated.

## Parameters

See [prometheus role documentation in kubernetes collection](
https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/-/blob/master/docs/roles/prometheus.md)
for a complete description.

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.k8s_platform.prometheus

- ansible.builtin.import_role:
    name: orange.k8s_platform.prometheus
  vars:
    kubernetes_charts_prometheus_version: 15.1.1
```
