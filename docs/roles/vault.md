# Vault

## Purpose

Vault deployment from official Helm Charts
<https://github.com/hashicorp/vault-helm>

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                | Purpose                                 | Default value                             |
|-----------------------------------------|-----------------------------------------|-------------------------------------------|
| `kubernetes_charts_vault_version`       | the vault helm charts version           | 0.11.0                                    |
| `kubernetes_charts_vault_namespace`     | the vault namespace                     | vault-system                              |
| `kubernetes_charts_vault_ha`            | to enable high availability             | false                                     |
| `kubernetes_charts_vault_tls`           | to enable TLS protocol                  | true                                      |
| `kubernetes_charts_vault_ui`            | to enable the vault UI                  | false                                     |
| `kubernetes_charts_vault_nodes`         | the vault nodes number for HA           | 3 in case of HA, 1 otherwise              |
| `kubernetes_charts_vault_storage_size`  | the vault storage size                  | 1Gi                                       |
| `kubernetes_charts_vault_storage_class` | the vault storage class                 | not set                                   |
| `kubernetes_charts_vault_token`         | vault token created with root privilege | undefined (from VAULT_TOKEN env variable) |
<!-- markdownlint-enable line-length -->

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.k8s_platform.vault

- ansible.builtin.import_role:
    name: orange.k8s_platform_vault
  vars:
    kubernetes_charts_vault_storage_size: 2Gi
```
