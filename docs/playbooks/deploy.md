# Deploy playbook

## Purpose

Install external requirements and install the different roles of this
collection.

## Inventory

The inventory must provide a group `helm` where helm binary will be installed
and helm charts installation process will be started.
The inventory must provide a group `k8s-cluster` where iScsi daemon will be
started.

This playbook mandates Ability to connect to the targeted Kubernetes via a kube
config file associated in default location
(`{{ ansible_user_dir }}/.kube/config`) for the chosen user on the server.

Search for vars files (`idf.yml` per default) happens at the same level as
inventory directory (see optional parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
```

## Mandatory parameters

A VAULT_TOKEN environment variable must exist for a vault deployment.
This variable enables to login to the vault instance

## Optional parameters

On top of all variables from roles, the playbook adds the following variables:

| Variable        | Purpose               | Default value            |
|-----------------|-----------------------|--------------------------|
| `property_file` | property file to load | `idf.yml`. Optional      |
| `base_dir`      | property file to load | `{{ inventory_dir }}/..` |

## Tags

The playbook proposes the following tags:

| Tag             | Purpose                                            |
|-----------------|----------------------------------------------------|
| `longhorn`      | for controlling longhorn installation              |
| `storage_class` | for controlling longhorn installation              |
| `vault`         | for controlling vault installation                 |
| `prometheus`    | for prometheus, alertmanager, grafana installation |

## Examples

Inventory:

```ini
[helm]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/deploy.yml

ansible-playbook -i inventory/inventory playbooks/deploy.yml --tags longhorn

ansible-playbook -i inventory/inventory playbooks/deploy.yml \
    --extra-vars "base_dir=~"

ansible-playbook -i inventory/inventory playbooks/deploy.yml \
    --extra-vars "{'kubernetes_docker_proxy': 'myProxy'}"
```
